# NRT Workshop 2023

The materials for the NRT ML workshop can be found in this repo and  detailed instructions are given below. 

## Overview of the workshop

- Presentation on the basics of Deep Learning and on how to write a neural network architecture using pytorch
- Understanding ML workflow - solving a linear regression problem.  
- An introduction to LIGO's denoising problem and relevant datasets
- Design a Convolutional Neural Network (CNN) for denoising
- Train the network and perform inference on the data

--------------------------

## Resource materials and useful links for getting ready for the workshop 

1. We will be using Google Colab for running python scripts. There are number of tutorials online on how to use google colab for your python programs (for example [this one](urlhttps://www.freecodecamp.org/news/google-colaboratory-python-code-in-your-google-drive/)). If you have not used Colab before, make sure you try it once before the workshop (just a 2-minute thing!).


1. If you have never used classes and objects in programming, it is recommended that you go over [this documentation](https://realpython.com/python3-object-oriented-programming/) and have some idea what they are and how they are used. 

1. Go over [this notebook](NRT_workshop_2023_warmup_material_00.ipynb) and learn the basic operations we do using `PyTorch`. Detailed instructions are given in the notebook. 

1. Go over [this notebook](NRT_workshop_2023_warmup_material_01.ipynb) and learn how `PyTorch` handles large datasets by dividing them into batches.  
